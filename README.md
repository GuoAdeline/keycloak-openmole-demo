# Keycloak - openmole

1. Run `keycloak` server : 
  ```
  git clone git@gitlab.com:GuoAdeline/keycloak-server.git
./keycloak-server/bin/standalone.sh -Djboss.socket.binding.port-offset=100
  ```
2. Run React web client :
 ```
 cd blog-keycloak/keycloak-react-part1
 npm start
 ```